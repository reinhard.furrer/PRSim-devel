# PRSim-devel

The R package `PRSim` provides a simulation framework to simulate streamflow time series with similar main characteristics as observed data. These characteristics include the distribution of daily streamflow values and their temporal correlation as expressed by short- and long-range dependence. The approach is based on the randomization of the phases of the Fourier transform and the randomization of the phases of the wavelet transform. We further use the flexible four-parameter Kappa distribution, which allows for the extrapolation to yet unobserved low and high flows.

This is the development repository. A recent stable version is posted on https://CRAN.R-project.org/package=PRSim
