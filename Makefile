## make update            # changing version number
## make lib               # install locally
## make tar               # create the tar file
## make check             # checking with $(CHECKARGS)

VERSION := 1.5
TARPACK = PRSim_$(VERSION).tar.gz
CHECKARGS =--as-cran


# --run-dontrun     do run \dontrun sections in the Rd files
# --run-donttest    do run \donttest sections in the Rd files
# --use-gctuse      'gctorture(TRUE)' when running examples/tests

## ----------------------------------------------------------------------------------


R_CHECK_ENVIRON:=check.Renviron

R:=R
RSCRIPT:=Rscript


all : tar

## update package version
update:
## only change the timestamps of the file(s) if necessary
	if [ "Version: $(VERSION)" != "$(shell grep 'Version' PRSim/DESCRIPTION)" ] ; then \
	cd PRSim && sed -i -r -- 's/^Version:.*/Version: '$(VERSION)'/g' DESCRIPTION ; \
	fi
	if [ "Date: $(shell date +'%F')" != "$(shell grep 'Date' PRSim/DESCRIPTION)" ] ; then \
	cd PRSim && sed -i -r -- 's/^Date:.*/Date: '`date +'%F'`'/g' DESCRIPTION ; \
	fi


## 'tar', 'lib' --------------------------
tar:  $(shell find PRSim -type f) Makefile
	$(MAKE) update
	mkdir -p tar
	cd tar && R_LIBS=../lib  $(R) CMD build ../PRSim



lib:  $(shell find PRSim -type f) Makefile 
	$(MAKE) update
	mkdir -p lib
	$(R) CMD INSTALL -l lib PRSim


## Check the package ------------------------------------
check: tar 
	cd tar &&  R_CHECK_ENVIRON=$(R_CHECK_ENVIRON) $(R) CMD check  $(CHECKARGS) $(TARPACK)

## winbuilder
check-win: update
	$(RSCRIPT) -e "devtools::check_win_release(pkg = \"PRSim\")"

check-win-old: update
	$(RSCRIPT) -e "devtools::check_win_oldrelease(pkg = \"PRSim\")"

check-win-devel: update
	$(RSCRIPT) -e "devtools::check_win_devel(pkg = \"PRSim\")"


## test package -------------------------------------------
## run tests including skip_on_cran() tests
#test-package: lib
#	$(RSCRIPT) -e "library(\"PRSim\", lib.loc = \"lib\"); devtools::test(\"PRSim\")"

## run examples including "dontrun example"
test-examples: lib
	$(RSCRIPT) -e "library(\"PRSim\", lib.loc = \"lib\");   devtools::run_examples(\"PRSim\", run_dontrun = FALSE)"


## test demos -------------------------------------------------
test-demos: lib
	rm -fr demoruns/demo
	cp -r PRSim/demo demoruns/
	cd demoruns && $(RSCRIPT) --vanilla rundemos.R > rundemos.Rout


## ----------------------------------------------------------

rm: clean
	rm -fr tar lib \
	PRSim/src/*.o PRSim/src/*.so PRSim/src/*.dll

clean:
	rm -f Rplots*.pdf .RData .Rhistory .RData
	rm -f demoruns/*.pdf demoruns/*out 
	rm -fr demoruns/demo


finalizer:
	cd tar && rm -rf PRSim.Rcheck/
	cd tar && R_LIBS=../lib $(R) CMD check $(TARPACK)
	cp tar/PRSim.Rcheck/PRSim-manual.pdf .
	cp -uv tar/PRSim.Rcheck/tests/*.Rout PRSim/tests/.
	cp -uv tar/PRSim.Rcheck/PRSim-Ex.Rout PRSim/tests/Examples/PRSim-Ex.Rout.save
	cd PRSim/tests/ && rm -f *.Rout.save && rename -v 's#\.Rout#\.Rout.save#' *.Rout
	cd tar && R_LIBS=../lib  $(R) CMD build ../PRSim


.PHONY: update tar lib \
	check check-win check-win-old check-win-devel \
	test-examples test-demos \
	rm clean finalizer


## Reinhard Furrer, spring 2023
